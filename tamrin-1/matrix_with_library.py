from matrixObj import Matrix, random_matrix

matrixA = random_matrix(-10, 10, (5, 5))
matrixB= random_matrix(-10, 10, (5, 5))

print("\nRandom matrix 1:\n ",matrixA,"\n")          # The Random matrix 2

print("The minor of the matrix:\n ",matrixA.minor(),"\n")          # The minor of the matrix

print("The Cofactor of the matrix:\n ", matrixA.cofactor(),"\n")       # The Cofactor of the matrix

print("Inverse of the matrix:\n ", matrixA.inverse(),"\n")        # Inverse of the matrix

print("Minor at row 1 column 1: ", matrixA.minor_at_ij(1, 1),"\n")      # Minor at row 1 column 1

print("Cofactor at row 3 column 1:  ", matrixA.cofactor_at_ij(3, 1),"\n")   # Cofactor at row 3 column 1 

print("Transfer Matrix to echelon form:\n  ", matrixA.rref(),"\n")   # Transfer Matrix to echelon form

print("Gets the reverse of the matrix rows with each row also reversed:\n ",matrixA[::-1, ::-1],"\n")     # Gets the reverse of the matrix rows with each row also reversed

print("Scaler Devision Matrix:\n  ", matrixA / 2,"\n")   # Scaler Devision Matrix

print("\nRandom matrix 2:\n ",matrixB,"\n")          # The Random matrix 2

print("Devision Matrix:\n  ", matrixA / matrixB,"\n")   # Devision Matrix